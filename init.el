; SPDX-License-Identifier: CC0-1.0

; Straight.el
(load (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory) nil 'nomessage)
; - Packages
; -- General
(straight-use-package 'deadgrep)
(straight-use-package 'helm)
(straight-use-package 'hl-todo)
(straight-use-package 'olivetti)
(straight-use-package 'magit)
(straight-use-package 'nswbuff)
(straight-use-package 'rainbow-delimiters)
(straight-use-package 'selectrum)
(straight-use-package 'undo-tree)
(straight-use-package 'vterm)

; -- Languages
(straight-use-package 'markdown-mode)
(straight-use-package 'go-mode)
(straight-use-package 'lua-mode)
(straight-use-package 'nim-mode)
(straight-use-package 'rust-mode)

; Global modes
; - Visual
(global-display-line-numbers-mode)
(add-hook 'text-mode-hook 'olivetti-mode)
(add-hook 'prog-mode-hook 'rainbow-delimiters-mode)
; - Text input
(selectrum-mode 1)
; - Undo/Redo
(global-undo-tree-mode 1)
; - Fuzzy search
(helm-mode 1)
; - Highlight some keywords
(global-hl-todo-mode 1)

; Look and feel
(set-face-attribute 'default nil :height 140 :font "Azeret Mono")
(setq-default line-spacing 0.5)

; Editing
(setq-default show-trailing-whitespace t)
; - Indentation
(setq-default indent-tabs-mode nil)
(setq-default tab-width 4)
(setq-default indent-line-function 'insert-tab)
(setq-default js-indent-level 2)
(setq-default ruby-indent-level 2)

; Key bindings
(windmove-default-keybindings 'control)
(global-set-key [C-tab] 'nswbuff-switch-to-next-buffer)
(global-set-key [C-iso-lefttab] 'nswbuff-switch-to-previous-buffer)
(global-set-key (kbd "C-x C-f") 'helm-find-files)

; Other
(setq-default inhibit-startup-screen t)
(setq-default create-lockfiles nil)
(setq-default initial-scratch-message nil)
(setq-default hl-todo-color-background t)
(setq-default hl-todo-keyword-faces
    '(("TODO"  . "#ffff7f")
      ("NOTE"  . "#aaff7f")
      ("HACK"  . "#55aaff")
      ("FIXME" . "#ff557f")))
