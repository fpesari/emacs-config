# emacs-config - My GNU Emacs configuration

This is my GNU Emacs configuration, which I want to share with
everyone, thankful to all the people who indirectly helped me
build it by sharing their knowledge on the web.

## License

Everything in this repo is released under the Creative Commons Zero (CC0) license.
